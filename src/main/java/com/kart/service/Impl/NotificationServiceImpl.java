/**
 * 
 */
package com.kart.service.Impl;

import java.util.List;
  
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kart.dao.NotificationRepository;
import com.kart.entity.Notification;
import com.kart.service.NotificationService;


/**
 * @author LENOVO
 *
 */

@Service
public class NotificationServiceImpl implements NotificationService {
	
	@Autowired
	private NotificationRepository notificationRepository;

	public List<Notification> getAllNotifications() {
		return notificationRepository.findAll();
	}

	public List<Notification> findAllByIds(Iterable<Long> ids) {
		return notificationRepository.findAllById(ids);
	}
	
	public List<Notification> findByNotificationName(String notificationName) {
		return notificationRepository.findByNotificationName(notificationName);
	}
	
	public Notification getOne(Long id) {
		return notificationRepository.getOne(id);
	}
	
	public Notification saveAndFlush(Notification notification) {
		return notificationRepository.saveAndFlush(notification);
	}

	public void flush() {
		notificationRepository.flush();
	}
	
	public List<Notification> saveAll(Iterable<Notification> notifications) {
		return notificationRepository.saveAll(notifications);
	}

	public void deleteInBatch(Iterable<Notification> notifications) {
		notificationRepository.deleteInBatch(notifications);
	}

	public void deleteAllInBatch() {
		notificationRepository.deleteAllInBatch();
	}
	
}
