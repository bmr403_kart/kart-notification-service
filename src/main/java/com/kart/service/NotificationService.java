package com.kart.service;

import java.util.List;

import com.kart.entity.Notification;



public interface NotificationService {
	
	List<Notification> getAllNotifications();

	List<Notification> findAllByIds(Iterable<Long> ids);

	Notification saveAndFlush(Notification notification);
	
	List<Notification> saveAll(Iterable<Notification> entities);
	
	void flush();
	
	Notification getOne(Long id);
	
	List<Notification> findByNotificationName(String productName);
	
	void deleteInBatch(Iterable<Notification> entities);

	void deleteAllInBatch();
	
}
