package com.kart.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.kart.entity.Notification;

@Repository
public interface NotificationRepository extends JpaRepository<Notification, Long>, CrudRepository<Notification, Long> {
	
	List<Notification> findByNotificationName(String notificationName);
	
}
